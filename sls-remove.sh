# Setup Serverless AWS Environment, creates aws credentials file in root dir
serverless config credentials --provider aws --key  $AWS_ACCESS_KEY_ID --secret $AWS_SECRET_ACCESS_KEY --profile serverless-cisco-admin

# Remove the AWS Lambda Function entirely, based on serverless.yml file
serverless remove -v

# Cleanup files created by serverless in root dir
rimraf /root/.serverless

rimraf /root/.aws

rm /root/.serverlessrc