# Setup Serverless AWS Environment, creates aws credentials file in root dir
serverless config credentials --provider aws --key  $AWS_ACCESS_KEY_ID --secret $AWS_SECRET_ACCESS_KEY --profile serverless-cisco-admin

# Deploy just the function code withouth rebuilding the entire Lambda stack
serverless deploy function -f vmanage-listener

# Cleanup files created by serverless in root dir
rimraf /root/.serverless

rimraf /root/.aws

rm /root/.serverlessrc