import requests
import json
import os
from rest_api import raise_for_status
from operator import itemgetter

# Rally API
RALLY_API_KEY = os.environ.get("rally_api_key")
RALLY_WORKSPACE = os.environ.get("rally_workspace")
RALLY_PROJECT = os.environ.get("rally_project")


class Rally:
    base_url = 'https://rally1.rallydev.com/slm/webservice/v2.0'
    api_key = RALLY_API_KEY
    workspace = RALLY_WORKSPACE
    project = RALLY_PROJECT

    @classmethod
    def defect_iter(cls):
        response = requests.get(f'{cls.base_url}/defect',
                                params={'workspace': f'{cls.base_url}/workspace/{cls.workspace}'},
                                verify=False, headers={'ZSESSIONID': cls.api_key})
        raise_for_status(response)

        # Return (<ref>, <object-id>, <name>) tuple
        entry_data = itemgetter('_ref', '_refObjectUUID', '_refObjectName')
        return (entry_data(entry) for entry in response.json().get('QueryResult', {}).get('Results', []))


class RallyDefect:
    def __init__(self, data):
        self.data = data

    @property
    def name(self):
        return self.data.get('Defect', {}).get('Name')

    @property
    def description(self):
        return self.data.get('Defect', {}).get('Description')

    @property
    def severity(self):
        return self.data.get('Defect', {}).get('Severity')

    @property
    def notes(self):
        return self.data.get('Defect', {}).get('Notes')

    @property
    def uuid(self):
        return self.data.get('Defect', {}).get('ObjectUUID')

    @property
    def formatted_id(self):
        return self.data.get('Defect', {}).get('FormattedID')

    @classmethod
    def get(cls, object_id):
        response = requests.get(f'{Rally.base_url}/defect/{object_id}',
                                params={'workspace': f'{Rally.base_url}/workspace/{Rally.workspace}'},
                                verify=False, headers={'ZSESSIONID': Rally.api_key})
        raise_for_status(response)

        return cls(response.json())

    @classmethod
    def create(cls, name, description, severity, notes='', return_field='FormattedID'):
        payload = {
            'Defect': {
                'Name': name,
                'Workspace': Rally.workspace,
                'Project': Rally.project,
                'Description': description,
                'Severity': severity,
                'Notes': notes
            }
        }
        response = requests.post(f'{Rally.base_url}/defect/create', json=payload, verify=False,
                                 headers={'ZSESSIONID': Rally.api_key})
        raise_for_status(response)

        result = response.json().get('CreateResult', {})

        if result.get('Errors', True) or result.get('Warnings', True):
            return None

        return result.get('Object', {}).get(return_field)

    def __str__(self):
        return json.dumps(self.data, indent=2)
