// SDWAN MONITORING WITH WEBHOOKS PROJECT

// Jenkins - https://engci-private-sjc.cisco.com/jenkins/sso-as/job/sandbox/job/Customers/job/CX-TTG-SRE-LAB/job/SDWAN-Monitoring-with-Webhooks-mb/

// SDWAN Monitoring Webhooks Webex Teams Notifications room
def WEBEX_ROOM_ID = "Y2lzY29zcGFyazovL3VzL1JPT00vNWY2M2RlNTAtNmI4ZS0xMWVhLTg0MmYtOGQ2YTQ0MGU4Y2Zh" 
def WEBEX_CREDENTIALS = "c6ef2037-6fab-4985-81c8-513463e8a164"
def EMAIL = "cx-ttg-sre-sdwan@cisco.com"

// GITHUB Vars
def GITHUB_CREDENTIALS = "345c79bc-9def-4981-94b5-d8190fdd2304"
def VERSION = "0.1.0"
def TAG
def COMMIT_ID_SHORT
def authorLastCommit
def errMsg
def scmVars
def stageName

// Sonarqube Vars
def sonarProjectKey = "cx-spade-sjc-403"
def sonarProjectName = "cx-spade-sjc-403"
// def sonarExludedDirs = ''
def sonarUrl = "https://engci-sonar-sjc.cisco.com/sonar/"

// AWS Lambda Vars
def AWS_CREDENTIALS = "f27461cb-78a5-4759-8ef4-98138029a67f"
def AWSconsole = "https://748232724340.signin.aws.amazon.com/console"

// Artifactory Vars
def ART_CREDENTIALS = "a819fdfe-dea2-4cb6-9106-b56f17bd8538"

// Use label/Region than a specific server name 'AMER-REGION'
node('AMER-REGION') {
    try {
        echo "BRANCH_NAME: ${BRANCH_NAME}"
        stage('Checkout Git') {
            stageName = env.STAGE_NAME
            scmVars = checkout([
                $class: 'GitSCM',
                branches: [[name: '**']],
                doGenerateSubmoduleConfigurations: false,
                extensions: [
                    [$class: 'WipeWorkspace'],
                    [$class: 'CleanBeforeCheckout']
                ],
                submoduleCfg: [],
                userRemoteConfigs: [[
                    credentialsId: GITHUB_CREDENTIALS,
                    url: 'https://wwwin-github.cisco.com/CX-TTG-SRE-LAB/SDWAN-Monitoring-with-Webhooks.git'
                ]]
            ])
            println(scmVars)
            COMMIT_ID_SHORT = scmVars.GIT_COMMIT[0..6]
            TAG = "${VERSION}-${COMMIT_ID_SHORT}"

            dir("${WORKSPACE}") {
                authorLastCommit = sh (
                    label: 'Getting Author of this commit', 
                    script: "git show -s --pretty=\"%an <%ae>\" ${scmVars.GIT_COMMIT} || true", 
                    returnStdout: true
                ).trim()
            }
            echo "Author: ${authorLastCommit}"
            sh 'ls -al'
            sh 'pwd'
        }
        docker.image("containers.cisco.com/cx-ttg-sre/pylinter").inside {
            stage('Static Code Analysis') {
                stageName = env.STAGE_NAME
                sh 'pwd'
                sh 'pip list'
                parallel (
                    'Pylint': {
                        withEnv(['PYLINTHOME=.']) {
                            sh 'pylint *.py || true'
                        }
                    },
                    'Flake8': {
                        // Check setup.cfg
                        sh 'flake8 . || true'
                    },
                    'Bandit': {
                        // Check setup.cfg
                        sh 'bandit -r --ini setup.cfg || true'
                    }
                )
            }
        }
        sh 'docker rmi -f containers.cisco.com/cx-ttg-sre/pylinter || true'
        stage('Quality Analysis') {
            stageName = env.STAGE_NAME
            if (BRANCH_NAME != "master") {
                echo "Skipping SONARQUBE QA for non-master Branch: Branch ${BRANCH_NAME}"
            } else {
                echo "BRANCH NAME = ${BRANCH_NAME}: Performing SONARQUBE QA"
            }
            if (BRANCH_NAME == "skipping master") {
                def scannerHome = tool 'SONARQUBE_HOME'

                withSonarQubeEnv('EngIT SJC SonarQube1') {
                    // sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName=${sonarProjectName} -Dsonar.projectVersion=${env.BUILD_TAG} -Dsonar.exclusions=${sonarExludedDirs} -Dsonar.sources=${env.workspace}"
                    sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName=${sonarProjectName} -Dsonar.projectVersion=${env.BUILD_TAG} -Dsonar.sources=${env.workspace}"
                }

                // Quality Gate
                timeout(time: 1, unit: 'HOURS') { // Just in case something goes wrong, pipeline will be killed after a timeout
                    def qg = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
                    if (qg.status != 'OK') {
                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                    }
                }
            }
        }
        //sh 'docker rmi -f containers.cisco.com/cx-ttg-sre/serverless || true'
        docker.image("containers.cisco.com/cx-ttg-sre/serverless").inside("-u root --entrypoint=''") {
            stage('Deploy Lambda Function') {
                stageName = env.STAGE_NAME
                    withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: AWS_CREDENTIALS , secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                        sh 'pwd'
                        sh 'ls -ltr'
                        sh './sls-update.sh'
                        sh 'ls -altr /root/'
                    }
            }
        }
        sh 'docker rmi -f containers.cisco.com/cx-ttg-sre/serverless || true'
        stage('Build'){
            if (BRANCH_NAME != "master") {
                echo "Skipping Build Stage for non-master Branch: Branch ${BRANCH_NAME}"
            } else {
                echo "BRANCH NAME = ${BRANCH_NAME}: Peforming Build Stage"
            }

            if (BRANCH_NAME == "master") {
            echo 'Build'
            // Create Tar file of tool to be uploaded to Artifactory
            sh '''
                    #!/bin/bash
                    pwd
                    tar -cvf SDWAN-Monitoring-with-Webhooks.tar ./handler.py ./serverless.yml
                '''
            }
        }
        stage('Publish'){
            if (BRANCH_NAME != "master") {
                echo "Skipping Publish Stage for non-master Branch: Branch ${BRANCH_NAME}"
            } else {
                echo "BRANCH NAME = ${BRANCH_NAME}: Peforming Publish Stage"
            }

            if (BRANCH_NAME == "skipping master") {

                // def server = Artifactory.newServer url: 'http://engci-maven.cisco.com/artifactory', username:'as-deployer',password:'yp41v2t9wiuanhfr'
                def server = Artifactory.newServer url: 'http://engci-maven.cisco.com/artifactory', credentialsId: ART_CREDENTIALS

                
                    def uploadSpec = """{
                        "files": [
                                    {
                                        "pattern": "*.tar",
                                        "target": "cx-ttg-sre-release/SDWAN-Monitoring-with-Webhooks/",
                                        "props": "p1=v1;p2=v2"
                                    }
                                ]
                    }"""
                    
                    def buildInfo1 = server.upload spec: uploadSpec
                    server.publishBuildInfo buildInfo1
            }
        }
            
        currentBuild.result = 'SUCCESS'
    } catch (err) {
        currentBuild.result = 'FAILURE'
        echo "Caught: ${err}"
        errMsg = err
        throw err
    } finally {
        // stage('Clean up') {
            // dir('automation/ada/') {
            //     sh "docker-compose -f docker-compose.common.yaml -f docker-compose.dev.yaml -f docker-compose.prod.yaml -f docker-compose.appd.yaml down --rmi all"
            // }
            // echo 'All clean'
            // sh label: 'List Docker images', script: 'docker image ls || true'
        // }
            stage('Notify') {
                def sparkMsg
                def emailMsg
                def buildDuration = currentBuild.durationString.minus(' and counting')

                if (currentBuild.result == 'SUCCESS') {
                    echo "Notify with SUCCESS message"
                    sparkMsg = "✅ **Build ${BUILD_ID} ${currentBuild.result}** <br/> **Jenkins Job**: [${JOB_NAME}](${BUILD_URL}) <br/> **Author**: ${authorLastCommit} <br/> **Build duration**: ${buildDuration} <br/> **Git Branch**: ${scmVars.GIT_BRANCH} <br/> **Git Commit**: ${scmVars.GIT_COMMIT} <br/> **GitHub URL**: ${scmVars.GIT_URL} </br> **Artifactory link**: https://engci-maven-master.cisco.com/artifactory/cx-ttg-sre-pypi-local/ </br> **SonarQube Report**: ${sonarUrl}dashboard?id=${sonarProjectKey} </br> **App Deployed to**: ${AWSconsole}"
                    emailMsg = """
                        <b>Job</b>: ${currentBuild.fullDisplayName}<br/>
                        <b>Author</b>: ${authorLastCommit}<br/>
                        <b>Status</b>: ${currentBuild.currentResult}<br/>
                        <b>Build duration</b>: ${buildDuration}</br>
                        <b>Git Branch</b>: ${scmVars.GIT_BRANCH}</br>
                        <b>Git Commit</b>: ${scmVars.GIT_COMMIT}</br>
                        <b>GitHub URL</b>: ${scmVars.GIT_URL}</br>
                        <b>Jenkins Build URL</b>: ${BUILD_URL}<br/>
                        <b>Artifactory link</b>: https://engci-maven-master.cisco.com/artifactory/cx-ttg-sre-pypi-local/</br>
                        <b>SonarQube Report</b>: ${sonarUrl}dashboard?id=${sonarProjectKey}<br/>
                        <b>App Deployed to</b>: ${AWSconsole}
                    """
                } else if (currentBuild.result == 'FAILURE') {
                    echo "Notify with FAILURE message"
                    sparkMsg = "❌ **Build ${BUILD_ID} ${currentBuild.result}** <br/> **Jenkins Job**: [${JOB_NAME}](${BUILD_URL}) <br/> **Failed Stage**: ${stageName} <br/> **Error**: ${errMsg}"
                    emailMsg = """
                        <b>Job</b>: ${currentBuild.fullDisplayName}<br/>
                        <b>Status</b>: ${currentBuild.result}<br/>
                        <b>Failed Stage</b>: ${stageName}</br>
                        <b>Error Message</b>: ${errMsg}</br>
                        <b>Build duration</b>: ${buildDuration}</br>
                        <b>Jenkins Build URL</b>: ${BUILD_URL}<br/>
                    """
                }
                parallel (
                    'Webex Teams': {
                        sparkSend (
                            credentialsId: WEBEX_CREDENTIALS,
                            failOnError: false,
                            messageType: 'markdown', 
                            spaceList: [[
                                spaceId: WEBEX_ROOM_ID
                                //spaceName: 'Collab ADA CICD Notifications'
                            ]],
                            message: sparkMsg
                        )
                    },
                    'Email': {
                        emailext (
                            mimeType: 'text/html',
                            subject: "${currentBuild.result}: SRE SDWAN Jenkins Build #${BUILD_ID}, Branch: ${BRANCH_NAME}",
                            body: emailMsg,
                            to: EMAIL
                            )
                        }
                    )
            }
        }
    }

