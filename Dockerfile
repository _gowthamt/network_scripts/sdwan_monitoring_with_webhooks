FROM node:lts-alpine
MAINTAINER gtamilse

# Install packages
RUN apk --no-cache update && \
    apk --no-cache add python python3==3.8.2-r0 python3-dev==3.8.2-r0 py-pip py-setuptools ca-certificates curl bash less wget unzip git openssh make tree jq && \
    update-ca-certificates

# Install awscli
RUN pip install --upgrade pip && \
    pip install --upgrade awscli

RUN rm -rf /var/cache/apk/*


# Install Serverless Framework
RUN npm install -g serverless

# Make Working Dir
RUN mkdir /app
WORKDIR /app

# Set Environement Variables
ENV awskey=abc
ENV awssecret=abc

ENTRYPOINT ["/bin/bash"]