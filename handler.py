import json
import os
import re
import requests
import logging.config
import time
from operator import itemgetter
from collections import namedtuple
from html import escape
from rest_api import Rest, LoginFailedException, RestAPIException, raise_for_status
from cmd_parser import dispatch_cmd, register_cmd, ParsingFailedException, CommandException
from ticket_infra import RallyDefect
from alarm_infra import register_alarm, handle_alarm, Alarm
from utils import Table
from uuid import uuid4
from datetime import datetime, timezone

# Disable self signed certificate warnings
import urllib3
urllib3.disable_warnings()

# Webex Teams API
BEARER_TOKEN = 'Bearer {}'.format(os.environ.get("bearer_token"))
ROOM_ID = os.environ.get("room_id")
WEBEX_BOT_WEBHOOK_ID = os.environ.get("webex_bot_webhook_id")

# vManage API
VMANAGE_IP = os.environ.get("vmanage_ip")
VMANAGE_PORT = os.environ.get("vmanage_port")
VMANAGE_USER = os.environ.get("vmanage_user")
VMANAGE_PASSWORD = os.environ.get("vmanage_password")
VMANAGE_TIMEOUT = 300
VMANAGE_URL = f'https://{VMANAGE_IP}:{VMANAGE_PORT}'

# Reason for setting level at chardet.charsetprober is to prevent unwanted debug messages from requests module
LOGGING_CONFIG = '''
{
    "version": 1,
    "formatters": {
        "simple": {
            "format": "%(levelname)s: %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "simple"
        }
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG"
    },
    "loggers": {
        "chardet.charsetprober": {
            "level": "INFO"
        }
    }
}
'''
logging.config.dictConfig(json.loads(LOGGING_CONFIG))
log = logging.getLogger()


# Webex Teams API
def teams_message_get(msg_id):
    response = requests.get(f'https://webexapis.com/v1/messages/{msg_id}',
                            verify=False, headers={'Authorization': BEARER_TOKEN})
    raise_for_status(response)
    return response.json()


def teams_message_post(msg, is_markdown=True):
    key = "markdown" if is_markdown else "text"
    response = requests.post('https://webexapis.com/v1/messages', json={"roomId": ROOM_ID, key: msg},
                             verify=False, headers={'Authorization': BEARER_TOKEN})
    raise_for_status(response)
    return response.json()
    


# Supporting functions
DeviceInfo = namedtuple('DeviceInfo', ['hostname', 'system_ip', 'uuid', 'type', 'model', 'version'])
DEVICE_INFO_FIELDS = ('host-name', 'system-ip', 'uuid', 'deviceType', 'deviceModel', 'version')


def get_device_info(api, system_ip=None, hostname=None):
    reply = api.get('system/device', f'vedges' if system_ip is None else f'vedges?deviceIP={system_ip}')

    device_info_iter = (DeviceInfo._make(device.get(field) for field in DEVICE_INFO_FIELDS)
                        for device in reply['data'])

    return (device_info for device_info in device_info_iter if hostname is None or device_info.hostname == hostname)


def is_ip_address(device_identifier):
    return re.match(r'\d+\.\d+\.\d+\.\d+$', device_identifier) is not None


def get_system_ip(api, device_identifier):
    if is_ip_address(device_identifier):
        return device_identifier

    for device_info in get_device_info(api, hostname=device_identifier):
        return device_info.system_ip

    raise CommandException(f'Device {device_identifier} does not exist')


# BOT commands
@register_cmd('show', 'system', ['hostname'])
def cmd_system(vmanage_api, args):
    for device_info in get_device_info(vmanage_api, hostname=args.hostname):
        return f'**system_ip:** {device_info.system_ip} **uuid:** {device_info.uuid} **version:** {device_info.version}'

    return f'**hostname not found**: {args.hostname}'


@register_cmd('show', 'control', ['device'])
def cmd_control(vmanage_api, args):
    device_id = get_system_ip(vmanage_api, args.device)
    reply = vmanage_api.get(f'device/control/synced/connections?deviceId={device_id}')
    color_set = {color['local-color'] for color in reply['data']}

    return 'Control Channel up on {size} transports.  Colors: {colors}'.format(
        size=len(color_set),
        colors=','.join(color_set)
    )


@register_cmd('show', 'interface', ['device'])
def cmd_interface(vmanage_api, args):
    device_id = get_system_ip(vmanage_api, args.device)
    reply = vmanage_api.get(f'device/interface?deviceId={device_id}')

    table = Table('Name', 'Status', 'VPN', 'Flaps', 'Description')
    table_data = (
        (entry['ifname'], entry['if-oper-status'], entry.get('vpn-id', ''), entry['num-flaps'], entry.get('desc', ''))
        for entry in reply['data'] if entry['af-type'] == 'ipv4'
    )
    table.extend(table_data)

    return '```  \n{table}  \n```'.format(table='  \n'.join(table.pretty_iter()))


@register_cmd('show', 'wan', ['device'])
def cmd_wan(vmanage_api, args):
    device_id = get_system_ip(vmanage_api, args.device)
    for device_info in get_device_info(vmanage_api, system_ip=device_id):
        params = '&'.join([
            f'deviceType={device_info.type}',
            f'deviceModel={device_info.model}',
            f'systemIp={device_info.system_ip}',
            f'localSystemIp={device_info.system_ip}',
            'reachability=reachable'
        ])
        return f'https://{VMANAGE_IP}:{VMANAGE_PORT}/index.html#/app/monitor/dashboard/wan/tunnel?{params}'

    raise CommandException(f'Device {args.device} does not exist')


@register_cmd('ticket', 'open', ['device', 'title'])
def cmd_ticket_open(vmanage_api, args):
    kwargs = {'system_ip': args.device} if is_ip_address(args.device) else {'hostname': args.device}
    d_info = next(get_device_info(vmanage_api, **kwargs), None)

    if d_info:
        context = [
            'Ticket opened via Webex Teams',
            f'Device {d_info.hostname} is a {d_info.model} {d_info.type} running {d_info.version}',
            f'System-IP: {d_info.system_ip}, UUID: {d_info.uuid}'
        ]
        ticket_id = RallyDefect.create(f'{d_info.hostname} - {args.title}',
                                       '<br />'.join(map(escape, context)),
                                       severity='moderate')
        if ticket_id:
            return f'Ticket created: {ticket_id}'

        raise CommandException(f'Failed to create ticket for {args.device}')

    return f'Device {args.device} was not found'


# vManage alarms
@register_alarm(['interface-state-change'])
def alarm_interface_state_change(alarm_obj: Alarm):
    lines = [
        f'**{alarm_obj.severity} Alarm:** {alarm_obj.pretty_type}',
    ]
    for name, interface, state in alarm_obj.values_iter(itemgetter('host-name', 'if-name', 'new-state')):
        lines.append(f'>**Device:** {name} **Interface:** {interface} **State:** {state}')

    return '\n\n '.join(lines), None


@register_alarm(['tloc_down', 'tloc_up'])
def alarm_tloc(alarm_obj: Alarm):
    lines = [
        f'**{alarm_obj.severity} Alarm:** {alarm_obj.pretty_type}',
    ]
    for name, site, color in alarm_obj.values_iter(itemgetter('host-name', 'site-id', 'color')):
        lines.append(f'>**Device:** {name} **Site:** {site} **TLOC Color:** {color}')

    return '\n\n '.join(lines), None


@register_alarm(['node_down', 'node_up'])
def alarm_node(alarm_obj: Alarm):
    lines = [
        f'**{alarm_obj.severity} Alarm:** {alarm_obj.pretty_type}',
    ]
    for name, site in alarm_obj.values_iter(itemgetter('host-name', 'site-id')):
        lines.append(f'>**Device:** {name} **Site:** {site}')

    if alarm_obj.type == 'node_down':
        node_list = ', '.join(alarm_obj.values_iter(itemgetter('host-name')))
        site_list = ', '.join(alarm_obj.values_iter(itemgetter('site-id')))
        description = [
            'Ticket opened via vManage Alarm',
            f'Alarm: {alarm_obj.message}'
        ]
        ticket_info = {
            'name': f'Node {node_list} at site {site_list} is down',
            'description': '<br />'.join(map(escape, description)),
            'severity': 'severe'
        }
    else:
        ticket_info = None

    return '\n\n '.join(lines), ticket_info


@register_alarm(['site_down', 'site_up'])
def alarm_site(alarm_obj: Alarm):
    lines = [
        f'**{alarm_obj.severity} Alarm:** {alarm_obj.pretty_type}',
    ]
    for site in alarm_obj.values_iter(itemgetter('site-id')):
        lines.append(f'>**Site:** {site}')

    if alarm_obj.type == 'site_down':
        site_list = ', '.join(alarm_obj.values_iter(itemgetter('site-id')))
        description = [
            'Ticket opened via vManage Alarm',
            f'Alarm: {alarm_obj.message}'
        ]
        ticket_info = {
            'name': f'Site {site_list} is down',
            'description': '<br />'.join(map(escape, description)),
            'severity': 'severe'
        }
    else:
        ticket_info = None

    return '\n\n '.join(lines), ticket_info


def is_user_command(event_data):
    return event_data.get('id', '') == WEBEX_BOT_WEBHOOK_ID and \
           'webex.bot' not in event_data.get('data', {}).get('personEmail', 'webex.bot')


# Main Function
def lambda_handler(event, context):
    event_data = json.loads(event['body'])
    log.debug('Function start: Event: %s', event_data)

    alarm = Alarm.parse(event_data)
    if alarm:
        log.debug('vManage alarm: %s', alarm.type)
        alarm_message, ticket_info = handle_alarm(alarm)
        teams_message = []
        if alarm_message:
            teams_message.append(alarm_message)
        if ticket_info:
            ticket_id = RallyDefect.create(**ticket_info)
            teams_message.append(f'**Ticket created:** {ticket_id}' if ticket_id else '**Failed to create ticket**')
        if teams_message:
            teams_message_post('\n\n '.join(teams_message))

    elif is_user_command(event_data):

        # Message from teams bot
        try:
            # raise ParsingFailedException()
            with Rest(VMANAGE_URL, VMANAGE_USER, VMANAGE_PASSWORD, timeout=VMANAGE_TIMEOUT) as api:
                cmd_tokens = teams_message_get(event_data['data']['id'])['text'].split()
                log.debug('Bot command received: %s', cmd_tokens)

                cmd_reply = dispatch_cmd(api, cmd_tokens)
                if cmd_reply is not None:
                    teams_message_post(cmd_reply)

        except (LoginFailedException, ConnectionError, RestAPIException) as ex:
            log.error(ex)
            teams_message_post('**vManage API error**: {msg}'.format(msg=escape(str(ex))))
        
        # RASA Changes
        except ParsingFailedException as ex:
            
            conversation_id = 'sre_teams_alarms' # ID shown in the RASA fw

            # pulling the auth token
            auth_header = {"username": "me","password": "cisco-rasa"}
            auth_url = 'http://10.0.0.165:8080/api/auth?token=XpHtpmHR3itbwgK'
            temp = requests.post(auth_url,json=auth_header)
            
            # checking the status token
            temp.raise_for_status()
            log.debug(temp.status_code)
            token = temp.json()['access_token']
            
            # get the current user's msg
            user_msg= str(teams_message_get(event_data['data']['id'])['text'][6:])
            out = '{\r\n  \"message\": \"'+user_msg+'\"\r\n}\r\n'
            
            # creating the URL to pull the current conversation
            post_url_rasa = str('http://10.0.0.165:8080/api/conversations/')
            post_url_rasa_end = str('/messages?token=XpHtpmHR3itbwgK')
            post_url_rasa = post_url_rasa + conversation_id + post_url_rasa_end
            
            # Sending the msg to the RASA server
            reply = requests.post(post_url_rasa, headers={'Authorization': 'Bearer '+token,'Content-Type': 'application/json'}, data=out)

            # Checking if the payload is a final msg
            if reply.text.find("custom") == -1:
                teams_message_post((reply.json())[0]['text'])
                log.debug("msg reply")
                
            # If the Final payload 
            if reply.text.find("custom") != -1:
                
                # pulling the current server name and ticket name
                server_name = reply.json()[1]['custom']['servername']
                ticket_name = reply.json()[1]['custom']['ticketname']
                
                # dict of commands ran by the server
                token_dict = {
                    'user_servername_form': ['sdwan','show','control',server_name], # show control <servername>
                    'help': ['sdwan','help'], # help
                    'user_servername_interface_form' : ['sdwan','show','interface',server_name], # show interface <servername>
                    'user_servername_wan_form' : ['sdwan','show','wan',server_name],  # show wan <servername>
                    'user_servername_ticket_form' : ['sdwan','ticket','open',server_name,ticket_name], # ticket open <servername>, <ticketname>
                    'user_system_form' :['sdwan','show','system',server_name], # show system <servername>

                }
                
                # matching the intent with the correct command
                with Rest(VMANAGE_URL, VMANAGE_USER, VMANAGE_PASSWORD, timeout=VMANAGE_TIMEOUT) as api:
                    cmd_tokens = token_dict.get((reply.json())[1]['custom']['intent'])
                    log.debug('Bot command received: %s', cmd_tokens)

                    cmd_reply = dispatch_cmd(api, cmd_tokens)
                    if cmd_reply is not None:
                        teams_message_post(cmd_reply)
                out = '{\r\n  \"message\": \"'+'/restart'+'\"\r\n}\r\n'
                requests.post(post_url_rasa, headers={'Authorization': 'Bearer '+token,'Content-Type': 'application/json'}, data=out)

        except CommandException as ex:
            log.info(ex)
            teams_message_post('**Command execution error**: {msg}'.format(msg=escape(str(ex))))

    
    return {
        'statusCode': 200,
        'body': 'requestID={req_id}'.format(req_id=event.get('requestId', 'no requestId'))
    }
