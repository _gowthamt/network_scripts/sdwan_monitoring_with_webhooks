import argparse
from typing import List, Callable, Iterable

TOP_CMD = 'sdwan'
ROOT_CMD_DICT = {
    'show': ('show', ),
    'ticket': ('ticket', ),
    'restart': ('restart', ),
}


def help_handler(api=None, args=None):
    choices = help_options[:]
    choices.append(f'{TOP_CMD} help')
    return 'Options are:  \n{choices}'.format(choices='  \n'.join(choices))


class ArgumentParser(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        raise ParsingFailedException(message or '')

    def error(self, message):
        raise ParsingFailedException(message)


class Join(argparse.Action):
    def __init__(self, option_strings, dest, delimiter=' ', **kwargs):
        self.delimiter = delimiter
        super().__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if isinstance(values, list):
            values = self.delimiter.join(values)

        setattr(namespace, self.dest, values)


def register_parser(base_subparsers, cmd_path: Iterable[str]):
    curr_subparsers = base_subparsers
    for cmd in cmd_path:
        curr_parser = curr_subparsers.add_parser(cmd)
        curr_subparsers = curr_parser.add_subparsers()

    return curr_subparsers


parser = ArgumentParser()
subparsers = parser.add_subparsers()
top_parser = subparsers.add_parser(TOP_CMD)
top_subparsers = top_parser.add_subparsers()
help_parser = top_subparsers.add_parser('help')
help_parser.set_defaults(handler=help_handler)

root_parser_dict = {root_cmd: register_parser(top_subparsers, root_path)
                    for root_cmd, root_path in ROOT_CMD_DICT.items()}
help_options = []


def register_cmd(cmd_root: str, cmd: str, cmd_args: List[str]):
    def decorator(handler_fn: Callable):
        root_parser = root_parser_dict.get(cmd_root)
        if root_parser is None:
            raise ParserException(f'command root {cmd_root} is not available.')

        new_parser = root_parser.add_parser(cmd)
        new_parser.set_defaults(handler=handler_fn)
        for arg in cmd_args[:-1]:
            new_parser.add_argument(arg, metavar=f'<{arg}>', nargs=1, action=Join)
        else:
            new_parser.add_argument(cmd_args[-1], metavar=f'<{cmd_args[-1]}>', nargs='+', action=Join)

        cmd_args_help = ', '.join(f'&lt;{arg}&gt;' for arg in cmd_args)
        help_options.append(f'{TOP_CMD} {cmd_root} {cmd} {cmd_args_help}')

        return handler_fn

    return decorator


def dispatch_cmd(api, cmd_tokens):
    try:
        args = parser.parse_args(cmd_tokens)
        return args.handler(api, args)
    except AttributeError:
        raise ParsingFailedException('Invalid command: "{cmd}", {detail}'.format(
            cmd=' '.join(cmd_tokens), detail=help_handler())
        )
    except ParsingFailedException as ex:
        raise ParsingFailedException('Invalid command: "{cmd}", {detail}'.format(
            cmd=' '.join(cmd_tokens), detail=ex)
        )


class CommandException(Exception):
    """ General command execution exception """
    pass


class ParserException(Exception):
    """ General parser exception """
    pass


class ParsingFailedException(ParserException):
    """ Exception for failed parsing of commands """
    pass
