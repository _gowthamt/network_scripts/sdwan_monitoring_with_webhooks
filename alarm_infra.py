import json
import re
from typing import List, Callable


class Alarm:
    _mandatory_keys = {
        'message',
        'component',
        'severity',
        'type',
        'entry_time',
        'values'
    }
    _acronyms = {
        'tloc'
    }

    def __init__(self, data):
        self.data = data

    def __getattr__(self, key):
        attr = self.data.get(key)
        if attr is None:
            raise AttributeError("'{cls_name}' object has no attribute '{attr}'".format(cls_name=type(self).__name__,
                                                                                        attr=key))
        return attr

    def values_iter(self, item_getter_fn):
        return (item_getter_fn(value) for value in self.values)

    @property
    def pretty_type(self):
        tokens = re.split(r'[_-]', self.type)
        tokens[0] = tokens[0].upper() if tokens[0] in self._acronyms else tokens[0].title()
        return ' '.join(tokens)

    def __str__(self):
        return json.dumps(self.data, indent=2)

    @classmethod
    def parse(cls, event_payload):
        if cls._mandatory_keys <= set(event_payload):
            return cls(event_payload)

        return None


_alarm_handlers = {
}


def register_alarm(types_handled: List[str]):
    def decorator(handler_fn: Callable):
        for entry in types_handled:
            _alarm_handlers[entry] = handler_fn

        return handler_fn

    return decorator


def handle_alarm(alarm_obj):
    handler = _alarm_handlers.get(alarm_obj.type)
    if handler is not None:
        return handler(alarm_obj)

    return None, None
