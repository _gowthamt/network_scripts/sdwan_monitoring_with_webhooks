# Disclaimer
**This function pack may be used internally but in order to deliver it to a customer the appropiate SOW must be in place.**

# **<p align="center">SDWAN MONITORING WITH WEBHOOKS</p>**

# Overview

- This tool was created provide real-time monitoring of Cisco SDWAN devices. 
- The tool utilizes the existing alarms and events notification features of vManage and provides insights beyond the vManage dashboard.
- API-centric tool integrates vManage alerts with external tools (AWS Lambda and Webex Teams) to better notify customers on network events.


# Contacts
## Authors

Gowtham Tamilselvan (gtamilse@cisco.com), Marcelo Reis (mareis@cisco.com), Robert Dwyer (robdwyer@cisco.com)

## Support

CX-TTG-SRE-SDWAN <cx-ttg-sre-sdwan@cisco.com>

# Version
*Specify the version of the package e.g.*

- handler.py - Version 0.1
- serverless.yml - Version 0.1

# Release Date
*Specify the realease date for the version stated above*
30 - June - 2020

# Use Case(s)
*Provide the use case or use cases that is/are covered with this package. You should use the following format in order to standardize the Use Case. The use case if included should include: Actors and Flow of Events, if possible include a diagram*
![Use Case Diagram](./UseCase_001.png)

# External Material
## Marketing
*Link to Doc Central where external and internal marketing material is located*

## SOW and Scoping
*Link to Doc Central where SOW and Scoping documents are located.*


# Installation
*Provide information on how to install the package. e.g.*

1. git clone project using `git clone http://github3.cisco.com/comunity/package.git`
2. execute `setup.py` inside cloned directory

# Usage

# Tags

Automation
